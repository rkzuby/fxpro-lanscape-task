package com.klyagin;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MainTest {

    //proper landscape
    int[] landscape = new int[]{5,2,3,4,5,4,0,3,1};

    @Test(expected = IllegalArgumentException.class)
    public void emptyLandscapeCauseIllegalArgumentException() {
        //given
        int[] badLandscape = new int[0];
        //when
        Main.calculateWaterAmount(badLandscape);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSmallLandscapeCauseIllegalArgumentException() {
        //given
        int[] smallLandscape = new int[2];
        //when
        Main.calculateWaterAmount(smallLandscape);
    }

    @Test(expected = IllegalArgumentException.class)
    public void landscapeWithNegativeValueCauseIllegalArgumentException() {
        //given
        int[] negativeLandscape = new int[]{5,3,3,-1};
        //when
        Main.calculateWaterAmount(negativeLandscape);
    }

    @Test
    public void calculationWithProperLandscapeReturnsExpectedValue() {
        //given
        long expectedValue = 9;

        //when
        long actualValue = Main.calculateWaterAmount(landscape);

        //then
        assertThat(actualValue, equalTo(expectedValue));
    }


}
