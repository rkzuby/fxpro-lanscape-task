package com.klyagin;

public class Main {
    public static void main(String[] args) {
        int[] landscape = new int[]{5,2,3,4,5,4,0,3,1};
        System.out.println(calculateWaterAmount(landscape));
    }

    public static long calculateWaterAmount(int[] landscape){
        long result = 0;
        if(landscape.length <= 2){
            throw new IllegalArgumentException("Landscape size should not be empty or smaller than 2");
        }
        for (int i = 0; i < landscape.length; i++) {
           if(landscape[i] < 0){
               throw new IllegalArgumentException("Landscape should not contain negative values");
           }
        }
        int[] leftSide = new int[landscape.length];
        leftSide[0] = 0;
        int leftSideMax = landscape[0];
        int rightSideMax = landscape[landscape.length - 1];

        for(int i=1; i < leftSide.length; i++){
            leftSide[i] = leftSideMax;
            if(landscape[i] >= leftSideMax)
                leftSideMax = landscape[i];
        }

        for(int i = landscape.length - 2; i >= 0; i--){
            long temp = 0;
            if((Math.min(leftSide[i], rightSideMax) - landscape[i]) > 0){
                temp = Math.min(leftSide[i], rightSideMax) - landscape[i];
            }else{
                temp = 0;
            }
            result += temp;
            if(landscape[i] >= rightSideMax)
                rightSideMax = landscape[i];
        }

        return result;

    }
}
